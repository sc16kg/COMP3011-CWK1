import json
import requests

session = requests.Session()


def get_module_list():
    response = session.get("http://sc16kg.pythonanywhere.com/modules/")
    data = response.json()
    print("-------- Name ------ Code -- Year -- Semester ------- Taught By ---------")
    for i in data.values():
        print(i)
        print("---------------------------------------------------------------------------")


def rate_professor(professor_id, module_code, year, semester, rating):
    try:
        payload = {"id": professor_id, "code": module_code, "year": year, "semester": semester, "rating": rating}
        response = session.post("http://sc16kg.pythonanywhere.com/rate/", data=json.dumps(payload))
        data = response.json()
        if "Type Error" in data:
            print("Type Error: Rating Must Be Type int")
        elif "Invalid Request" in data:
            print("Request type should be POST")
        elif "Not Authenticated" in data:
            print("Please login to rate professors")
        elif "Rating Error" in data:
            print("Rating Error: Rating must be between 1 and 5 (inclusive).")
        elif "Does Not Exist" in data:
            print("One or more of the supplied fields do not exist in the database.")
        elif "Rating" in data:
            print("Rating Successful")
    except json.decoder.JSONDecodeError:
        print("An Error has Occurred.")


def average_rating(professor_id, module_code):
    try:
        response = session.get("http://sc16kg.pythonanywhere.com/rating/average/{professor_id}/{code}/".format(
            professor_id=professor_id, code=module_code))
        data = response.json()
        if "Does Not Exist" in data:
            print("One or more of the supplied fields does not exist in the database")
        elif "No Ratings" in data:
            print("{professor} has no ratings for module {module}".format(
                professor=data["Professor"], module=data["Module"]))
        elif "Rating" in data:
            print("The rating of {professor} in module {module} is {rating}".format(
                professor=data["Professor"], module=data["Module"], rating=data["Rating"]))
    except json.decoder.JSONDecodeError:
        print("An Error has Occurred.")


def view_ratings():
    try:
        response = session.get("http://sc16kg.pythonanywhere.com/rating/all/")
        data = response.json()
        if "Does Not Exist" in data:
            print("No Professors")
        else:
            for d in data.values():
                print("The rating of {professor} is {rating}".format(professor=d["Professor"], rating=d["Rating"]))
    except json.decoder.JSONDecodeError:
        print("An Error has Occurred.")


def login():
    username = input("Username: ")
    password = input("Password: ")
    try:
        data = {"Username": username, "Password": password}
        response = session.post("http://sc16kg.pythonanywhere.com/login/", data=json.dumps(data))
        data = response.json()
        if "Error" in data:
            print("An Error Occurred")
        elif "Logged In Successfully" in data:
            print("Logged in successfully")
        elif "User Does Not Exist" in data:
            print("User Does Not Exist")
    except json.decoder.JSONDecodeError:
        print("An Error has Occurred.")


def logout():
    try:
        response = session.post("http://sc16kg.pythonanywhere.com/logout/")
        data = response.json()
        if "Success" in data:
            print("Logged Out Successfully")
        else:
            print("An Error Has Occurred")
    except json.decoder.JSONDecodeError:
        print("An Error has Occurred.")


def register():
    username = input("Username: ")
    email = input("Email: ")
    password = input("Password: ")
    repassword = input("Re-Enter Password: ")
    try:
        if password == repassword:
            data = {"Username": username, "Email": email, "Password": password}
            response = session.post("http://sc16kg.pythonanywhere.com/create/", data=json.dumps(data))
            data = response.json()
            if "Error" in data:
                print("An Error Occurred")
            elif "Success" in data:
                print("User registered successfully")
        else:
            print("Passwords do not match")
    except json.decoder.JSONDecodeError:
        print("An Error has Occurred.")


if __name__ == "__main__":
    print("------------------------------------------")
    print("-  Rate Professors  Client Application   -")
    print("-  Use command 'help' for command list   -")
    print("------------------------------------------")
    while True:
        command = input()
        command = command.split()
        if len(command) > 0:
            if command[0] == "list":
                get_module_list()
            elif command[0] == "view":
                view_ratings()
            elif command[0] == "rate":
                if len(command) == 6:
                    rate_professor(command[1], command[2], command[3], command[4], int(command[5]))
                else:
                    print("Not Enough Arguments")
            elif command[0] == "average":
                if len(command) == 3:
                    average_rating(command[1], command[2])
                else:
                    print("Not Enough Arguments")
            elif command[0] == "register":
                register()
            elif command[0] == "login":
                login()
            elif command[0] == "logout":
                logout()
            elif command[0] == "quit":
                break
            elif command[0] == "help":
                print("--------- COMMANDS ----------")
                print("'list' - Used to view a list of all module instances and the professor(s) teaching each them")
                print("'view' - Used  to view  the  rating  of  all professors")
                print("'average professor_id module_code' - Used to view the average rating of a certain professor in a certain module")
                print("'rate professor_id module_code year semester rating' - Used to rate the teaching of a certain professor in a certain module instance")
                print("'register' - used to register to the service using a username, email and a password")
                print("'quit' - Close the application")
            else:
                print("Not a valid command. Use command 'help' for command list.")
