from django.apps import AppConfig


class RateprofessorsapiConfig(AppConfig):
    name = 'RateProfessorsAPI'
