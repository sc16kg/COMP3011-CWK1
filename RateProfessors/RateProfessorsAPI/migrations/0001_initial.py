# Generated by Django 3.0.4 on 2020-03-12 18:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Module',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('module_code', models.CharField(max_length=4, unique=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='ModuleInstance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('academic_year', models.CharField(max_length=4)),
                ('semester', models.PositiveSmallIntegerField(choices=[[1, 1], [2, 2]])),
                ('module_code', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='RateProfessorsAPI.Module')),
            ],
        ),
        migrations.CreateModel(
            name='Professor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('professor_id', models.CharField(max_length=3, unique=True)),
                ('firstname', models.CharField(max_length=20)),
                ('lastname', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rating', models.PositiveSmallIntegerField(choices=[[1, 1], [2, 2], [3, 3], [4, 4], [5, 5]])),
                ('module_instance', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='RateProfessorsAPI.ModuleInstance')),
                ('professor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='RateProfessorsAPI.Professor')),
            ],
        ),
        migrations.AddField(
            model_name='moduleinstance',
            name='taught_by',
            field=models.ManyToManyField(to='RateProfessorsAPI.Professor'),
        ),
    ]
