from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Professor(models.Model):
    professor_id = models.CharField(max_length=3, unique=True)
    firstname = models.CharField(max_length=20)
    lastname = models.CharField(max_length=30)

    def __str__(self):
        string = "Professor {initial}. {lastname} ({professor_id})".format(professor_id=self.professor_id,
                                                                        initial=self.firstname[0],
                                                                        lastname=self.lastname)
        return string


class Module(models.Model):
    module_code = models.CharField(max_length=4, unique=True)
    name = models.CharField(max_length=50)

    def __str__(self):
        string = "{name} ({module_code})".format(
            module_code=self.module_code,
            name=self.name)
        return string


class ModuleInstance(models.Model):
    module_code = models.ForeignKey('Module', on_delete=models.CASCADE)
    academic_year = models.CharField(max_length=4)
    semester = models.PositiveSmallIntegerField(choices=[[1, 1], [2, 2]])
    taught_by = models.ManyToManyField(Professor)

    def __str__(self):
        string = "{code} {year} {semester} ".format(code=self.module_code, year=self.academic_year,
                                                               semester=self.semester)
        taught_by = list(self.taught_by.all())
        for i, prof in enumerate(taught_by):
            if i < len(taught_by) - 1:
                string = string + str(prof) + " & "
            else:
                string = string + str(prof)
        return string


class Rating(models.Model):
    module_instance = models.ForeignKey(ModuleInstance, on_delete=models.CASCADE)
    professor = models.ForeignKey(Professor, on_delete=models.CASCADE)
    rating = models.PositiveSmallIntegerField(choices=[[1, 1], [2, 2], [3, 3], [4, 4], [5, 5]])


