from rest_framework import serializers
from .models import Professor, Module, Rating, ModuleInstance

class ProfessorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Professor
        fields = ('firstname', 'lastname', 'professor_id')

class ModuleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Module
        fields = ('module_code', 'name')

class RatingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Rating
        fields = ('module_instance', 'professor', 'rating')

class ModuleInstanceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ModuleInstance
        fields = ('module_code', 'academic_year', 'semester', 'taught_by')