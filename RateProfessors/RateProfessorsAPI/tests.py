from django.test import TestCase
from .models import Professor, Module, Rating, ModuleInstance

# Create your tests here.

class ProfessorTestCase(TestCase):
    def setUp(self):
        p1 = Professor.objects.create(firstname="John", lastname="Excellent", professor_id="JE1")
        p2 = Professor.objects.create(firstname="Terry", lastname="Terrible", professor_id="TT1")
        p3 =Professor.objects.create(firstname="Very", lastname="Smart", professor_id="VS1")
        m1 = Module.objects.create(module_code="CD1", name="Computing for Dummies")
        m2 = Module.objects.create(module_code="PG1", name="Programming for the Gifted")

    def test_professor(self):
        p1 = Professor.objects.get(professor_id="JE1")
        p2 = Professor.objects.get(professor_id="TT1")
        p3 = Professor.objects.get(professor_id="VS1")
        self.assertEqual(str(p1), "Professor J. Excellent (JE1)")
        self.assertEqual(str(p2), "Professor T. Terrible (TT1)")
        self.assertEqual(str(p3), "Professor V. Smart (VS1)")

    def test_module(self):
        m1 = Module.objects.get(module_code="CD1")
        m2 = Module.objects.get(module_code="PG1")
        self.assertEqual(str(m1), "Computing for Dummies (CD1)")
        self.assertEqual(str(m2), "Programming for the Gifted (PG1)")