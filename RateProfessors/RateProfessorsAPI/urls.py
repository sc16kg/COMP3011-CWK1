# myapi/urls.py

from django.urls import include, path
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(r'Professors', ProfessorViewSet)
router.register(r'Modules', ModuleViewSet)
router.register(r'Ratings', RatingViewSet)
router.register(r'Module Instances', ModuleInstanceViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('create/', create_user),
    path('login/', login_request),
    path('logout/', logout_request),
    path('modules/', modules_list),
    path('rate/', rate_professor),
    path('rating/average/<str:id>/<str:code>/', average_rating),
    path('rating/all/', list_ratings)
]