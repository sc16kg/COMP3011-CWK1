from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, decorators
from .serializers import ProfessorSerializer, ModuleSerializer, RatingSerializer, ModuleInstanceSerializer
from .models import Professor, Module, Rating, ModuleInstance
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import auth
import json
from django.http import HttpResponse, JsonResponse


# Create your views here.
class ProfessorViewSet(viewsets.ModelViewSet):
    queryset = Professor.objects.all().order_by('professor_id')
    serializer_class = ProfessorSerializer


class ModuleViewSet(viewsets.ModelViewSet):
    queryset = Module.objects.all().order_by('module_code')
    serializer_class = ModuleSerializer


class RatingViewSet(viewsets.ModelViewSet):
    queryset = Rating.objects.all().order_by('module_instance')
    serializer_class = RatingSerializer


class ModuleInstanceViewSet(viewsets.ModelViewSet):
    queryset = ModuleInstance.objects.all().order_by('module_code')
    serializer_class = ModuleInstanceSerializer


@csrf_exempt
def create_user(request):
    if request.method == "POST":
        try:
            body = request.body.decode("utf-8")
            body = json.loads(body)
            try:
                username = body["Username"]
                password = body["Password"]
                email = body["Email"]
            except IndexError:
                data = {"Incomplete": True}
                JsonResponse(data, status=400)
            user = User.objects.create_user(username=username, email=email, password=password)
            data = {"Success": str(user)}
            return JsonResponse(data, status=201)
        except json.decoder.JSONDecodeError:
            data = {"Error": True}
            return JsonResponse(data, status=400)
    else:
        data = {"Error": True}
        return JsonResponse(data, status=400)


@csrf_exempt
def login_request(request):
    if request.method == 'POST':
        body = request.body.decode("utf-8")
        body = json.loads(body)
        username = body["Username"]
        password = body["Password"]
        user = authenticate(request, username=username, password=password)
    else:
        data = {"Invalid Request": True}
        return JsonResponse(data, status=400)
    if user is not None:
        if user.is_active:
            login(request, user)
            request.session['username'] = username

            if user.is_authenticated:
                data = {"Logged In Successfully": True}
                return JsonResponse(data, status=200)
            else:
                data = {"Error": True}
                return JsonResponse(data, status=400)
        else:
            data = {"Error": True}
            return JsonResponse(data, status=400)
    else:
        data = {"User Does Not Exist": True}
        return JsonResponse(data, status=400)


@csrf_exempt
def logout_request(request):
    if request.method == 'POST':
        logout(request)
        data = {"Success": True}
        return JsonResponse(data, status=200)
    else:
        data = {"Invalid Request": True}
        return JsonResponse(data, status=400)


def modules_list(request):
    if request.method == "GET":
        modules = {}
        for i, module in enumerate(ModuleInstance.objects.all()):
            modules[i] = str(module)
        return JsonResponse(modules, status=200)
    else:
        data = {"Invalid Request": True}
        return JsonResponse(data, status=400)


@csrf_exempt
def rate_professor(request):
    user = auth.get_user(request)
    if user.is_authenticated:
        if request.method == "POST":
            try:
                body = request.body.decode("utf-8")
                body = json.loads(body)
                id = body["id"]
                code = body["code"]
                year = body["year"]
                semester = body["semester"]
                rating = body["rating"]
                data = {}
                p = Professor.objects.filter(professor_id=id)
                if len(p) == 0:
                    data["Does Not Exist"] = True
                    return JsonResponse(data, status=400)
                else:
                    p = p[0]
                    m = Module.objects.filter(module_code=code)
                    if len(m) == 0:
                        data["Does Not Exist"] = True
                        return JsonResponse(data, status=400)
                    else:
                        m = m[0]
                        mi = ModuleInstance.objects.filter(module_code=m, academic_year=year, semester=semester,
                                                           taught_by=p)
                        if len(mi) == 0:
                            data["Does Not Exist"] = True
                            return JsonResponse(data, status=400)
                        else:
                            mi = mi[0]
                            if type(rating) == int:
                                if 0 < rating < 6:
                                    rate = Rating(module_instance=mi, professor=p, rating=rating)
                                    rate.save()
                                    data["Rating"] = str(rate)
                                    return JsonResponse(data, status=201)
                                else:
                                    data["Rating Error"] = True
                                    return JsonResponse(data, status=400)
                            else:
                                data["Type Error"] = True
                                return JsonResponse(data, status=400)
            except json.decoder.JSONDecodeError:
                data = {"Error": True}
                return JsonResponse(data, status=400)
        else:
            data = {"Invalid Request": True}
            return JsonResponse(data, status=400)
    else:
        data = {"Not Authenticated": True}
        return JsonResponse(data, status=400)


def average_rating(request, id, code):
    if request.method == "GET":
        data = {}
        p = Professor.objects.filter(professor_id=id)
        if len(p) == 0:
            data["Does Not Exist"] = True
            return JsonResponse(data, status=400)
        else:
            p = p[0]
            data["Professor"] = str(p)
            m = Module.objects.filter(module_code=code)
            if len(m) == 0:
                data["Does Not Exist"] = True
                return JsonResponse(data, status=400)
            else:
                m = m[0]
                data["Module"] = str(m)
                mi = ModuleInstance.objects.filter(module_code=m, taught_by=p)
                if len(mi) == 0:
                    data["Does Not Exist"] = True
                    return JsonResponse(data, status=400)
                else:
                    total_ratings = 0
                    for instance in mi:
                        ratings = Rating.objects.filter(professor=p, module_instance=instance)
                        if len(ratings) == 0:
                            data["No Ratings"] = True
                            return JsonResponse(data, status=400)
                        else:
                            total_ratings += len(ratings)
                            for rating in ratings:
                                if "Rating" in data:
                                    data["Rating"] += rating.rating
                                else:
                                    data["Rating"] = rating.rating
                    data["Rating"] = data["Rating"] / total_ratings
                    data["Rating"] = asterisk_rating(data["Rating"])
                    return JsonResponse(data, status=200)
    else:
        return HttpResponse("Invalid Request")


def list_ratings(request):
    if request.method == "GET":
        data = {}
        professors = Professor.objects.all()
        if len(professors) == 0:
            data["Does Not Exist"] = True
            return JsonResponse(data, status=400)
        else:
            for i, p in enumerate(professors):
                rate = {}
                rate["Professor"] = str(p)
                ratings = Rating.objects.filter(professor=p)
                if len(ratings) != 0:
                    for rating in ratings:
                        if "Rating" in rate:
                            rate["Rating"] += rating.rating
                        else:
                            rate["Rating"] = rating.rating
                    rate["Rating"] = rate["Rating"] / len(ratings)
                    rate["Rating"] = asterisk_rating(rate["Rating"])
                    data[i] = rate
                else:
                    rate["Rating"] = ""
                    data[i] = rate
            return JsonResponse(data, status=200)

    else:
        return HttpResponse("Invalid Request")


def asterisk_rating(rating):
    tmp = round((rating % 1) + 1) - 1
    if tmp == 1 and round(rating) < rating:
        rating = round(rating) + 1
    else:
        rating = round(rating)
    string = ""
    for i in range(rating):
        string += "*"
    return string
